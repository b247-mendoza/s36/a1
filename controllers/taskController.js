const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}

// The request body coming from the client was passed from the "taskRoute.js" file via "req.body" as an argument and is renamed a "requestBody" parameter in the controller file.
module.exports.createTask = (requestBody) => {

	// Creates a task object based ont he Mongoose model "task"
	let newTask = new Task({
		
		// Sets the "name" property with the value received fromt he client/postman
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
})

module.exports.updateTask = (taskId, newContent) => {
	
	// The findById, is a mongoose method that will look for a task with the same id provided from the URL.
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		
		// Result of the "findById" method will be stored in the result parameter.
		result.name = newContent.name;
		
		// Saves the updated object in the MongoDB database
		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}


// finding and retrieving one task
module.exports.getOneTask = (taskId) => {
	return Task.findOne(taskId).then((result, err) => {
			if (err) {
				console.log("No task found in the ID given");
			} else{
				return result;
			}
		})
}

// changing status of one task
module.exports.updateOneTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		result.status = "completed";

		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}