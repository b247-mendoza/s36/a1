const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

// [ SECTION ] Get all tasks
router.get("/", (req, res) => {
	
	// It invokes the getAllTasks function from the taskControll.js
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// [ SECTION ] Create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// [ SECTION ] Delete a task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// [ SECTION ] Update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res. send(resultFromController));
})

// Get a specific Task
router.get("/:d", (req, res) => {
	taskController.getOneTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// Update a status of a specific task
router.put("/:id/complete", (req,res) => {
	taskController.updateOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
})
module.exports = router;